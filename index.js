//Iteración #1: Buscar el máximo

//Completa la función que tomando dos números como argumento devuelva el más alto.

function sum(numberOne , numberTwo) {
    if (numberOne > numberTwo) {
        document.write("El número mayor es :" + numberOne);
        } else {
          document.write("El número mayor es :" + numberTwo);
        }
};


//Iteración #2: Buscar la palabra más larga

/*Completa la función que tomando un array de strings como argumento devuelva el más largo, en caso de que dos strings tenga la misma longitud deberá devolver el primero.

Puedes usar este array para probar tu función:*/

const avengers = ['Hulk', 'Thor', 'IronMan', 'Captain A.', 'Spiderman', 'Captain M.'];
function findLongestWord (words) {
  return words.reduce((longest_word, current_word) => {
    return longest_word.length > current_word.length ? longest_word : current_word;
  });
};

console.log(findLongestWord(avengers));

/*Iteración #3: Calcular la suma

Calcular una suma puede ser tan simple como iterar sobre un array y sumar cada uno de los elementos.
Implemente la función denominada sumNumbers que toma un array de números como argumento y devuelve la suma de todos los números de la matriz. 
*/


const numbers = [1, 2, 3, 5, 45, 37, 58];
let total = numbers.reduce((a, b) => a + b, 0);

console.log(total);


/*Iteración #4: Calcular el promedio

Calcular un promedio es una tarea extremadamente común. Puedes usar este array para probar tu función:*/


const numbers = [12, 21, 38, 5, 45, 37, 6];
function average(list) {
    let sum = 0;
    var b = list.length ;
 
    if(b == 0){
        return 0
    } else{
        for (let i = 0; i < b; i++) {
            sum = (sum + list[i]);
        }
        let prom = sum/b
        return prom
    }
} 
console.log(average([12, 21, 38, 5, 45, 37, 6]));


/*Iteración #6: Valores únicos**

Crea una función que reciba por parámetro un array y compruebe si existen elementos duplicados, en caso que existan los elimina para retornar un array sin los elementos duplicados. Puedes usar este array para probar tu función:*/

const duplicates = [
  'sushi',
  'pizza',
  'burger',
  'potatoe',
  'pasta',
  'ice-cream',
  'pizza',
  'chicken',
  'onion rings',
  'pasta',
  'soda'
];
let findDuplicates = arr => arr.filter((item, index) => arr.indexOf(item) != index)

console.log(findDuplicates(duplicates)) 

let sinRepetidos = duplicates.filter((valorActual, indiceActual, arreglo) => {
    return arreglo.findIndex(valorDelArreglo => JSON.stringify(valorDelArreglo) === JSON.stringify(valorActual)) === indiceActual
});
console.log("Sin repetidos es:", sinRepetidos);